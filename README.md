# eines

Algunes solucions caseres ràpides que m'he hagut de fer per que no les trobo per internet

lletres
-------
Em creo una pseudo-tipografia amb lletres dibuixades pels meus fills i faig que surtin colors de fons alternatius 

menus
-----
Generació de menus setmanals i llistes de la compra.
Més tard l'he convertit en un plugin de WordPress, veure "[My Meals Scheduler](https://framagit.org/kusinkay/my-meals-scheduler)"
