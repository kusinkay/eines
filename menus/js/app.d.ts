/// <reference path="../typescript/jquery.d.ts" />
declare class Utils {
    /**
     * If len is greater than array.length, there could be repeating items
     */
    static rand(array: Array<object>, len?: number): object[];
}
declare class Apats {
    plats: Array<object>;
    disponibles: Array<object>;
    seleccionats: Array<object>;
    ingredients: Object;
    dies: Array<string>;
    constructor(params: object);
    /**
     * Tanca el dialeg de funcions de l'àpat
     */
    private tanca_apat();
    /**
     * Veure el seguent plat disponible
     */
    private seguent();
    private trobar(cerca, emprar?);
    private target(apat, dia);
    private buida(apat, dia);
    private afegir_boto(target, valors);
    /**
     *
     * @param apat (0: dinar, 1: sopar)
     * @param dia ( 1-7: dilluns-diumenge )
     * @param id identificador de plat, si es buit agafem el darrer
     */
    private emprar(apat, dia, id?, add?);
    private ingredient(ingredient);
    /**
     *
     * @param recalcular es recalculara en base als plats seleccionats
     */
    private llistar_ingredients(recalcular?);
    private llistar_coccions();
    private resum_disponibles();
    private refrescar();
    private actualitza_qr(qr);
    /**
     * Treu l'assignació actual i intenta asignar-li un altre disponible
     * @param apat
     * @param dia
     */
    private assignar(apat, dia);
    /**
     * Cerca un plat complementari, si el palt obtingut es un primer o un segon
     * @param apat
     * @param dia
     * @param plat
     * @param cerca
     */
    private complementari(apat, dia, plat, cerca);
    /**
     * Troba un plat alternatiu en funcio del public (adult/nen)
     * @param apat
     * @param dia
     * @param plat
     * @param cerca
     */
    private alternatiu(apat, dia, plat, cerca);
    /**
     * informa de les posicions que ocupa l'actual apat dins l'array de seleccionats
     * per recuperar-los mes tard.
     *
     * @param apat
     * @param dia
     * @param id identificador de plat
     */
    private alliberar(apat, dia, id?);
    private recuperar(pos);
    /**
     *
     * @param dia
     */
    private dinar(dia);
    test(): void;
}
