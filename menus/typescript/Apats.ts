/// <reference path="jquery.d.ts" />
/// <reference path="Utils.ts" />
class Apats {
    plats: Array<object>;

    disponibles: Array<object>;
    seleccionats: Array<object> = new Array<object>();

    ingredients: Object = {};

    dies: Array<string> = ['', 'dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte', 'diumenge'];

    constructor( params: object ) {
        var self = this;
        $.ajax( {
            dataType: "json",
            url: "data/plats.json",
            success: function( json ) {
                /** 
                 * set identifiers */
                for ( var i = 0; i < json.length; i++ ) {
                    json[i].id = i + 1;
                }
                self.plats = json;
                self.ingredients = {};
                var index = 0;
                self.disponibles = Utils.rand( self.plats );
                for ( var apat = 0; apat <= 1; apat++ ) {
                    for ( var dia = 1; dia <= 7; dia++ ) {

                        self.assignar( apat, dia );

                    } //dies
                }//apats
                self.refrescar();
                $( '[data-cell]' ).off().on( 'click', function() {
                    /*
                    var parts = $( this ).attr( 'data-cell' ).split( '|' );
                    var apat = parseInt( parts[0] );
                    var dia = parseInt( parts[1] );
                    $( '#apat .apat' ).html(( apat == 0 ? 'dinar' : 'sopar' ) + '' );
                    $( '#apat .dia' ).html( self.dies[dia] );
                    $( '#apat button' ).attr( 'data-apat', apat );
                    $( '#apat button' ).attr( 'data-dia', dia );
                    $( '.reveal-overlay' ).show();
                    $( '.reveal' ).show();
                    */
                } );
                if ( navigator == null || navigator.clipboard == undefined ) {
                    $( '#copia-ingredients' ).hide();
                }
                if ( typeof params.loaded == 'function' ) {
                    params.loaded();
                }
            }
        } );

        $( '#apats thead tr th' ).off().on( 'click', function() {
            var ara = 'festiu';
            var despres = 'laborable';
            if ( $( this ).hasClass( 'laborable' ) ) {
                ara = 'laborable';
                despres = 'festiu';
            }
            if ( !$( this ).hasClass( 'capdesetmana' ) ) {
                $( this ).removeClass( ara );
                $( this ).addClass( despres );
            }
        } );

        $( '#tanca-apat' ).click( function() {
            self.tanca_apat();
        } )
    };

    /**
     * Tanca el dialeg de funcions de l'àpat
     */
    private tanca_apat() {
        $( '.reveal-overlay' ).hide();
        $( '.reveal' ).hide();
    }

    /**
     * Veure el seguent plat disponible
     */
    private seguent() {
        return this.disponibles[this.disponibles.length - 1];
    }

    private trobar( cerca: object, emprar?: boolean ): any {
        for ( var i = 0; i < this.disponibles.length; i++ ) {
            var plat = this.disponibles[i];
            var trobat = true;
            for ( var attr of Object.keys( cerca ) ) {
                if ( cerca.hasOwnProperty( attr ) ) {
                    var verificar = ( cerca[attr] == plat[attr] ) || ( cerca[attr] == null ) && !plat.hasOwnProperty( attr );
                    if ( Array.isArray( cerca[attr] ) ) {
                        verificar = false;
                        for ( var values = 0; values < cerca[attr].length; values++ ) {
                            verificar = verificar || ( plat[attr] == cerca[attr][values] ) || ( cerca[attr][values] == null ) && !plat.hasOwnProperty( attr );
                        }
                    }
                    trobat = trobat && verificar;
                }
            }
            if ( trobat ) {
                var resultat = this.disponibles[i];
                if ( emprar ) {
                    resultat = this.disponibles.splice( i, 1 )[0];
                }
                console.debug( 'trobat (' + JSON.stringify( cerca ) + ')' );
                console.debug( resultat );
                return resultat;
            }
        }
        console.error( 'NO trobat (' + JSON.stringify( cerca ) + ')' );
        return false;
    }

    private target( apat: number, dia: number ) {
        return '#apats tbody tr:eq(' + apat + ') td:eq(' + dia + ')';
    }

    private buida( apat: number, dia: number ) {
        var self = this;
        var target = this.target( apat, dia );
        $( target ).html( '' );

        this.afegir_boto( target, {
            label: 'Crear àpat',
            class: 'crea-plats',
            data: {
                apat: apat,
                dia: dia
            }
        } );

        $( '.crea-plats' ).off().on( 'click', function() {
            var apat = parseInt( $( this ).attr( 'data-apat' ) );
            var dia = parseInt( $( this ).attr( 'data-dia' ) );
            self.assignar( apat, dia );
        } )
    }

    private afegir_boto( target, valors: object ) {
        var cellbutton = true;
        var tag = 'span';
        if ( valors.tag != null ) {
            tag = valors.tag;
            cellbutton = false;
        }
        var content = '&nbsp;';
        if ( valors.content != null ) {
            content = valors.content;
        }
        var button = $( '<' + tag + '>' );
        $( button ).attr( 'title', valors.label );
        $( button ).addClass( valors.class );
        if ( cellbutton ) {
            $( button ).addClass( 'button' );
        }
        $( button ).html( content );
        for ( var key of Object.keys( valors.data ) ) {
            $( button ).attr( 'data-' + key, valors.data[key] );
        }
        $( target ).prepend( button );
    }

    /**
     * 
     * @param apat (0: dinar, 1: sopar)
     * @param dia ( 1-7: dilluns-diumenge )
     * @param id identificador de plat, si es buit agafem el darrer
     */
    private emprar( apat: number, dia: number, id?: string, add?: boolean ) {
        var self = this;
        if ( add == null ) {
            add = false;
        }
        var plat: object;
        if ( id == null ) {
            console.debug( 'emprem darrer' );
            plat = this.disponibles.splice( -1, 1 )[0];
        } else {
            plat = this.trobar( { id: id }, true );
        }
        if ( plat != false ) {
            plat.emprat = {
                apat: apat,
                dia: dia
            };
            var variacio = '';
            if ( plat.variacio != null ) {
                plat.variacio = Utils.rand( plat.variacio );
                variacio = plat.variacio[0].nom;
            }
            var target = '#apats tbody tr:eq(' + apat + ') td:eq(' + dia + ')';
            var public = '';
            if ( plat.public != null ) {
                public = ( plat.public != null && plat.public == 'adult' ? '(a)' : '(n)' );
            }
            var ordre = '';
            if ( plat.ordre != null ) {
                ordre = plat.ordre + ( plat.ordre == 1 ? 'r' : 'n' ) + ': '
            }
            var caption = ordre + public + plat.titol + ' ' + variacio;
            var dplat = $( '<div>' );
            $( dplat ).attr( 'class', 'plat' );
            $( dplat ).attr( 'data-plat-id', plat.id );
            $( dplat ).html( caption );

            this.afegir_boto( dplat, {
                content: 'x',
                tag: 'button',
                label: 'Treure plat',
                class: 'elimina-plat',
                data: {
                    apat: apat,
                    dia: dia,
                    id: plat.id
                }
            } );

            if ( !add ) {
                $( target ).html( '' );
            }
            $( target ).append( dplat );
            //$( target ).html(( add ? $( target ).html() + '<br>' : '<br>' ) + caption );
            $( target ).attr( 'data-cell', apat + '|' + dia )

            this.seleccionats.push( plat );
            if ( $( '.button', $( target ) ).length == 0 ) {
                this.afegir_boto( target, {
                    label: 'Afegir plat',
                    class: 'selecciona-plat',
                    data: {
                        apat: apat,
                        dia: dia
                    }
                } );

                this.afegir_boto( target, {
                    label: 'Canviar àpat',
                    class: 'canvia-plats',
                    data: {
                        apat: apat,
                        dia: dia
                    }
                } );

                this.afegir_boto( target, {
                    label: 'Esborrar àpat',
                    class: 'elimina-plats',
                    data: {
                        apat: apat,
                        dia: dia
                    }
                } );
            }
        } else {
            console.warn( 'plat NO trobat: ' + id );
        }
    }

    private ingredient( ingredient: object ) {
        if ( !this.ingredients.hasOwnProperty( ingredient.nom ) ) {
            ingredient.quantitat = 1;
            this.ingredients[ingredient.nom] = ingredient;
        } else {
            this.ingredients[ingredient.nom].quantitat++;
        }
    }
    /**
     * 
     * @param recalcular es recalculara en base als plats seleccionats
     */
    private llistar_ingredients( recalcular?: boolean ) {
        var self = this;
        if ( recalcular == null ) {
            recalcular = true;
        }
        if ( recalcular ) {
            this.ingredients = {};
            for ( var i = 0; i < this.seleccionats.length; i++ ) {
                var plat = this.seleccionats[i];
                if ( plat.variacio != null ) {
                    var ingredients = plat.variacio[0].ingredients;
                    if ( ingredients != null ) {
                        for ( var j = 0; j < ingredients.length; j++ ) {
                            this.ingredient( ingredients[j] );
                        }
                    }
                }
                for ( var k = 0; k < plat.ingredients.length; k++ ) {
                    var ingredient = plat.ingredients[k];
                    this.ingredient( ingredient );
                }
            }
        }
        $( '#ingredients' ).html( '' );
        var qr = '';
        for ( var key of Object.keys( this.ingredients ).sort() ) {
            var ingredient = this.ingredients[key];
            var div = $( '<div>' );
            $( div ).html( ingredient.quantitat + ' x ' + ingredient.nom );
            qr += ingredient.nom + '\n';
            $( div ).attr( 'class', 'ingredient' );
            var delbtn = $( '<button>' );
            $( delbtn ).attr( 'data-del-ingredient', key );
            $( delbtn ).html( 'x' );
            $( div ).prepend( delbtn );
            $( '#ingredients' ).append( div );
        }

        this.actualitza_qr( qr );

        $( '[data-del-ingredient]' ).off().on( 'click', function() {
            delete self.ingredients[$( this ).attr( 'data-del-ingredient' )];
            self.llistar_ingredients( false );
        } );
    }

    private llistar_coccions() {
        var valors = {};
        var totals = 0;
        for ( var i = 0; i < this.seleccionats.length; i++ ) {
            var plat = this.seleccionats[i];
            var coccio = null;
            if ( plat.coccio != null ) {
                coccio = plat.coccio;
            }
            if ( plat.variacio != null ) {
                coccio = plat.variacio[0].coccio;
            }
            if ( coccio != null ) {
                if ( valors[coccio.tipus] == null ) {
                    valors[coccio.tipus] = 1;
                } else {
                    valors[coccio.tipus]++;
                }
                totals++;
            }
        }//for
        var html = '';
        for ( var key of Object.keys( valors ) ) {
            html += Math.round( valors[key] / totals * 100 ) + '% - ' + key + ':  ' + valors[key] + '<br>';
        }
        $( '#coccions' ).html( html );
        console.debug( valors );
    }

    private resum_disponibles() {
        var valors = {
            apat: {},
            public: {},
            ordre: {},
            restriccio: {}
        };
        for ( var i = 0; i < this.disponibles.length; i++ ) {
            var plat = this.disponibles[i];
            var apat = 'indiferent';
            if ( plat.apat != null ) {
                apat = plat.apat;
            }
            valors.apat[apat] = ( valors.apat[apat] == null ? 1 : valors.apat[apat] + 1 );

            var tpublic = 'indiferent';
            if ( plat.public != null ) {
                tpublic = plat.public;
            }
            valors.public[tpublic] = ( valors.public[tpublic] == null ? 1 : valors.public[tpublic] + 1 );

            var ordre = 'únics';
            if ( plat.ordre != null ) {
                ordre = ( plat.ordre == 1 ? 'primers' : 'segons' );
            }
            valors.ordre[ordre] = ( valors.ordre[ordre] == null ? 1 : valors.ordre[ordre] + 1 );

            var restriccio = 'resta';
            if ( plat.restriccio != null ) {
                restriccio = ( plat.restriccio == 'capdesetmana' ? 'festiu' : plat.restriccio );
            }
            valors.restriccio[restriccio] = ( valors.restriccio[restriccio] == null ? 1 : valors.restriccio[restriccio] + 1 );
        }
        var tpublic = '';
        for ( var key of Object.keys( valors.public ) ) {
            tpublic += valors.public[key] + ' ' + key + '<br>';
        }
        $( '#plats-public' ).html( tpublic );

        var apat = '';
        for ( var key of Object.keys( valors.apat ) ) {
            apat += valors.apat[key] + ' ' + key + '<br>';
        }
        $( '#plats-apat' ).html( apat );

        var ordre = '';
        for ( var key of Object.keys( valors.ordre ) ) {
            ordre += valors.ordre[key] + ' ' + key + '<br>';
        }
        $( '#plats-ordre' ).html( ordre );

        var restriccio = '';
        for ( var key of Object.keys( valors.restriccio ) ) {
            restriccio += valors.restriccio[key] + ' ' + key + '<br>';
        }
        $( '#plats-restriccio' ).html( restriccio );
        console.debug( valors );
    }

    private refrescar() {
        var self = this;
        this.llistar_ingredients();
        this.llistar_coccions();
        this.resum_disponibles();


        $( '.canvia-plats, .elimina-plats' ).off().on( 'click', function() {
            var apat = parseInt( $( this ).attr( 'data-apat' ) );
            var dia = parseInt( $( this ).attr( 'data-dia' ) );
            var pos = self.alliberar( apat, dia );
            if ( $( this ).hasClass( 'canvia-plats' ) ) {
                self.assignar( apat, dia );
            } else {
                self.buida( apat, dia );
            }
            self.recuperar( pos );
            self.refrescar();
            self.tanca_apat();
        } );

        $( '.elimina-plat' ).off().on( 'click', function() {
            var apat = parseInt( $( this ).attr( 'data-apat' ) );
            var dia = parseInt( $( this ).attr( 'data-dia' ) );
            var id = parseInt( $( this ).attr( 'data-id' ) );
            var pos = self.alliberar( apat, dia, id );
            $( '[data-plat-id=' + id + ']' ).remove();
            self.recuperar( pos );
            self.refrescar();
        } );

        $( '.selecciona-plat' ).off().on( 'click', function() {
            var apat = parseInt( $( this ).attr( 'data-apat' ) );
            var dia = parseInt( $( this ).attr( 'data-dia' ) );
            var dades = {};
            for ( var i = 0; i < self.disponibles.length; i++ ) {
                var plat = self.disponibles[i];
                var key = 'únics populars';
                if ( plat.ordre != null ) {
                    key = ( plat.ordre == 1 ? 'primers' : 'segons' );
                } else {
                    if ( plat.public != null ) {
                        key = 'únics ' + ( plat.public );
                    }
                }
                if ( dades[key] == null ) {
                    dades[key] = new Array<object>();
                }
                dades[key].push( { id: plat.id, titol: plat.titol } );
            }
            var target = self.target( apat, dia );
            if ( $( '.selector-plat', $( target ) ).length == 0 ) {
                var select = $( '<select>' );
                $( select ).attr( 'class', 'selector-plat' );
                $( target ).prepend( select );
            } else {
                $( target + ' .selector-plat' ).html( '' );
                $( target + ' .selector-plat' ).show();
            }
            $( target + ' .selector-plat' ).append( $( '<option>' ) );
            for ( var key of Object.keys( dades ) ) {
                var optgroup = $( '<optgroup>' );
                $( optgroup ).attr( 'label', key );

                dades[key].sort( function( a, b ) {
                   if ( a.titol > b.titol ){
                       return 1;
                   } else if ( a.titol < b.titol ){
                       return -1;
                   }
                   return 0;
                } );
                for ( var i = 0; i < dades[key].length; i++ ) {
                    var plat = dades[key][i];
                    var option = $( '<option>' );
                    $( option ).val( plat.id );
                    $( option ).html( plat.titol );
                    $( optgroup ).append( option );
                }

                $( target + ' .selector-plat' ).append( optgroup );
            }

            $( '.selector-plat' ).off().on( 'change', function() {
                $( this ).hide();
                self.emprar( apat, dia, $( this ).val(), true );
                self.refrescar();
            } );
        } );

        $( '#copia-ingredients' ).off().on( 'click', function() {
            document.execCommand( "copy" );

            /* Get the text field */
            var copyText = document.getElementById( "msg" );

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange( 0, 99999 ); /* For mobile devices */
            if ( navigator != null && navigator.clipboard != undefined ) {
                navigator.clipboard.writeText( copyText.value ).then(
                    function() {
                        alert( "Els ingredients s'han copiat al porta-papers en format text." );
                    },
                    function() {
                        alert( "Disculpa, no s'han pogut copiar els ingredients al porta-papers." );
                    }
                );
            } else {
                /* Copy the text inside the text field */
                //document.execCommand( "copy" );
                alert( "La copia al porta-retalls no està disponible" );
            }
        } );
    }

    private actualitza_qr( qr: string ) {
        $( '#msg' ).val( qr );
        update_qrcode();
    }

    /**
     * Treu l'assignació actual i intenta asignar-li un altre disponible
     * @param apat
     * @param dia
     */
    private assignar( apat: number, dia: number ) {
        var plat = false;
        var cerca: object = {
            apat: ( apat == 0 ? ['dinar', null] : ['sopar', null] )
        };
        if ( $( '#apats thead th:eq(' + dia + ')' ).hasClass( 'festiu' ) ) {
            cerca.restriccio = 'capdesetmana';
        } else {
            cerca.restriccio = null;
        }
        plat = this.trobar( cerca );
        if ( plat != false ) {
            this.emprar( apat, dia, plat.id );
            /**
             * plat complementari */
            this.complementari( apat, dia, plat, cerca );
            /*plat complementari*/
            if ( plat.ordre == null ) {
                //les variants de 1er/2n alternatives les tractem dins el complementari
                this.alternatiu( apat, dia, plat, cerca );
            }

        } else {
            console.warn( 'plat NO trobat (' + apat + ', ' + dia + '): ' + JSON.stringify( cerca ) );
        }
        this.refrescar();
    }

    /**
     * Cerca un plat complementari, si el palt obtingut es un primer o un segon
     * @param apat
     * @param dia
     * @param plat
     * @param cerca
     */
    private complementari( apat: number, dia: number, plat: object, cerca: object ) {
        if ( plat.ordre != null ) {
            /*  buscar un plat complementari */
            cerca.ordre = ( plat.ordre == 1 ? 2 : 1 );
            plat = this.trobar( cerca );
            if ( plat != false ) {
                this.emprar( apat, dia, plat.id, true );
                this.alternatiu( apat, dia, plat, cerca );
            }
        }
    }

    /**
     * Troba un plat alternatiu en funcio del public (adult/nen)
     * @param apat
     * @param dia
     * @param plat
     * @param cerca
     */
    private alternatiu( apat: number, dia: number, plat: object, cerca: object ) {
        if ( plat.public != null ) {
            var unic = false;
            if ( plat.ordre == null ) {
                //havia sortit un plat unic
                unic = true;
                cerca.ordre = null;
            }
            cerca.public = ( plat.public == 'adult' ? ( unic ? null : 'nen' ) : 'adult' );
            plat = this.trobar( cerca );
            if ( plat != false ) {
                this.emprar( apat, dia, plat.id, true );
            }
        }
    }

    /**
     * informa de les posicions que ocupa l'actual apat dins l'array de seleccionats
     * per recuperar-los mes tard.
     * 
     * @param apat
     * @param dia
     * @param id identificador de plat
     */
    private alliberar( apat: number, dia: number, id?: number ) {
        var result: Array<number> = new Array<number>();
        for ( var i = 0; i < this.seleccionats.length; i++ ) {
            var plat = this.seleccionats[i];
            if ( plat.emprat.apat == apat && plat.emprat.dia == dia ) {
                if ( id == null || plat.id == id ) {
                    result.push( i );
                }
            }
        }
        return result;
    }

    private recuperar( pos: Array<number> ) {
        for ( var i = 0; i < pos.length; i++ ) {
            this.disponibles = this.disponibles.concat( this.seleccionats.splice( pos[i], 1 ) );
        }
    }

    /**
     * 
     * @param dia
     */
    private dinar( dia: number ) {

    }

    public test() {
        console.debug( this.plats );
    }
}