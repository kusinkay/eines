class Utils {
    /**
     * If len is greater than array.length, there could be repeating items
     */
    public static rand(array: Array<object>, len?: number) {
        var assignat: Array<object> = new Array<object>();
        if (len != null && len != array.length) {
            for (var i = 0; i < len; i++) {
                var r = Math.floor((Math.random() * (array.length) - 1) + 1);
                assignat[i] = array[r];
            }
        } else {
            for (var i = 0; i < array.length; i++) {

                var r = Math.floor((Math.random() * (array.length) - 1) + 1);
                while (assignat[r] != undefined) {
                    r = Math.floor((Math.random() * (array.length) - 1) + 1);
                }
                assignat[r] = array[i];
            }
        }
        return assignat;
    }
}